KRoots is copyright by Jonathan Fine (were.Vire@gmail.com), 2008-2009
KRoots is copyright by DanSoft (dik@inbox.ru), 2018

This program is licensed under the terms and conditions of
the GNU General Public License (GPLv3+); either version
3 of the License, or (at your option) any later version.
Please read the 'COPYING' file for more information.

To build KRoots you need KF 5.x, Qt5 and cmake.
Full list of dependencies: see pkg/kroots.spec

Building:
cmake ..
make

Installation (under root):
make install

Translations:
https://www.transifex.com/Magic/kroots
