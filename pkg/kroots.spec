Name:           kroots
Version:        1.6.1
Release:        %mkrel 1
Summary:        Find all the roots of a complex number
Group:          Education
License:        GPLv3+
Url:            https://bitbucket.org/admsasha/kroots
Source0:        https://bitbucket.org/admsasha/kroots/downloads/%{name}-%{version}.tar.gz

BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-macros
BuildRequires:  kcoreaddons-devel
BuildRequires:  ki18n-devel
BuildRequires:  kconfig-devel
BuildRequires:  kplotting-devel
BuildRequires:  kxmlgui-devel
BuildRequires:  kdoctools-devel
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(openssl)
BuildRequires:  poxml

Recommends:     khelpcenter

%description
KRoots finds all the roots of a complex number and graphs
them on the real/imaginary axis. It supports both the a + bi
format and the r cis o format. KRoots can also be used to
create regular polygons.

%prep
%setup -q

%build
%cmake_kf5
%make_build

%install
%make_install -C build
%find_lang %{name}

%files -f %{name}.lang
%doc README*
%license COPYING
%{_kf5_docdir}/HTML/*/%{name}
%{_kf5_applicationsdir}/%{name}.desktop
%{_kf5_bindir}/%{name}
%{_kf5_datadir}/config.kcfg/%{name}.kcfg
%{_kf5_datadir}/kxmlgui5/%{name}/%{name}ui.rc
%{_kf5_iconsdir}/hicolor/*/apps/%{name}.*
