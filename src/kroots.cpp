/*
 * kroots.cpp
 *
 * Copyright (C) 2008-2009 Jonathan Fine (were.Vire@gmail.com)
 * Copyright (C) 2018 DanSoft (dik@inbox.ru)
 *
 * This file is part of KRoots.
 *
 * KRoots is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KRoots is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KRoots.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "kroots.h"
#include "krootsview.h"
#include "settings.h"

#include <KConfigDialog>

#include <QAction>
#include <KActionCollection>
#include <KStandardAction>
#include <KAcceleratorManager>
#include <QMenuBar>
#include <QStatusBar>

#include <KLocalizedString>

KRoots::KRoots()
	: KXmlGuiWindow(),
	  mView(new KRootsView(this))
{
	setCentralWidget(mView);
	setupActions();
	setupGUI();
	
	//Get rid of stupid settings menus that are not needed
	foreach(QAction * item, menuBar()->actions().at(1)->menu()->actions())
		if (!item->text().contains(i18n("KRoots")))
			delete item;

	//delete (hide) switching of languages from menu
	delete menuBar()->actions().at(3)->menu()->actions().at(5);
	
	//kill the status bar
	statusBar()->deleteLater();
}

KRoots::~KRoots()
{
}

void KRoots::setupActions()
{
	KStandardAction::quit(qApp, SLOT(closeAllWindows()), actionCollection());
	KStandardAction::preferences(this, SLOT(optionsPreferences()), actionCollection());
	
    QAction *a = actionCollection()->addAction(QLatin1String("clear"));
	a->setText(i18n("Clear Graph"));
	connect( a,SIGNAL(triggered( bool)), mView, SLOT(clear()));
	a->setEnabled( true );
	
    a = actionCollection()->addAction(QLatin1String("draw"));
	a->setText(i18n("Draw the Roots"));
	connect( a,SIGNAL(triggered( bool)), mView, SLOT(drawPolygon()));
	a->setEnabled( true );
}

void KRoots::optionsPreferences()
{
    if ( KConfigDialog::showDialog(QLatin1String( "settings" )) )  return;
	
    KConfigDialog *dialog = new KConfigDialog(this, QLatin1String("settings"), Settings::self());
	QWidget *generalSettingsDlg = new QWidget;
	ui_prefs_base.setupUi(generalSettingsDlg);
    dialog->addPage(generalSettingsDlg, i18n("General"), QLatin1String("preferences-other"));


    QList<QWidget*> l = generalSettingsDlg->findChildren<QWidget *>();
    for (int i=0;i<l.size();i++){
        QWidget *w = l[i];
        if (!w->toolTip().isEmpty()) w->setToolTip(i18n(w->toolTip().toStdString().c_str()));
        if (!w->whatsThis().isEmpty()) w->setWhatsThis(i18n(w->whatsThis().toStdString().c_str()));

        QLabel *lbl = qobject_cast<QLabel*>(w);
        if (lbl) lbl->setText(i18n(lbl->text().toStdString().c_str()));

        QCheckBox *ckb = qobject_cast<QCheckBox*>(w);
        if (ckb) ckb->setText(i18n(ckb->text().toStdString().c_str()));

        QRadioButton *rbt = qobject_cast<QRadioButton*>(w);
        if (rbt) rbt->setText(i18n(rbt->text().toStdString().c_str()));

    }

	
	connect(dialog, SIGNAL(settingsChanged(QString)), mView, SLOT(settingsChanged()));
	dialog->setAttribute( Qt::WA_DeleteOnClose );
	dialog->show();
}

#include "kroots.moc"
