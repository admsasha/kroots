/*
 * main.cpp
 *
 * Copyright (C) 2008-2009 Jonathan Fine (were.Vire@gmail.com)
 * Copyright (C) 2018 DanSoft (dik@inbox.ru)
 *
 * This file is part of KRoots.
 *
 * KRoots is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KRoots is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KRoots.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "kroots.h"
#include <QApplication>
#include <KAboutData>
#include <KLocalizedString>

static const char description[] =
	I18N_NOOP("A Program that calculates all of the roots of a complex number and graphs them!");

static const char version[] = "1.6.1";

int main(int argc, char **argv)
{
	QApplication app(argc,argv);
	KLocalizedString::setApplicationDomain("kroots");

    KAboutData about(QLatin1String("kroots"), i18n("KRoots"), QLatin1String(version), i18n(description),
					KAboutLicense::GPL_V3, i18n("(C) 2008-2009 Jonathan Fine, (C) 2018 DanSoft"),
                    QString(), QString(), QLatin1String("https://bitbucket.org/admsasha/kroots/issues"));
    about.addAuthor( i18n("Jonathan Fine"), QString(), QLatin1String("were.Vire@gmail.com"));
    about.addAuthor( QLatin1String("DanSoft"), QString(), QLatin1String("dik@inbox.ru"));

	KAboutData::setApplicationData(about);

    KRoots *widget = new KRoots;
	
	// see if we are starting with session management
	if (app.isSessionRestored())
	{
		RESTORE(KRoots);
	} else widget->show();
	
	return app.exec();
}
