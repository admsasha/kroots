/*
 * kroots.h
 *
 * Copyright (C) 2008-2009 Jonathan Fine (were.Vire@gmail.com)
 * Copyright (C) 2018 DanSoft (dik@inbox.ru)
 *
 * This file is part of KRoots.
 *
 * KRoots is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KRoots is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KRoots.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef KROOTS_H
#define KROOTS_H

#include <KXmlGuiWindow>
#include "ui_prefs_base.h"

class KRootsView;

class KRoots : public KXmlGuiWindow
{
	Q_OBJECT
public:
	KRoots();
	virtual ~KRoots();

private Q_SLOTS:
	void optionsPreferences();

private:
	void setupActions();

private:
	Ui::prefs_base ui_prefs_base ;
	KRootsView *mView;
};

#endif // _KROOTS_H_
