/*
 * krootsview.cpp
 *
 * Copyright (C) 2008-2009 Jonathan Fine (were.Vire@gmail.com)
 * Copyright (C) 2018 DanSoft (dik@inbox.ru)
 *
 * This file is part of KRoots.
 *
 * KRoots is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * KRoots is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KRoots.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "krootsview.h"
#include "settings.h"

#include <KLocalizedString>
#include <QLabel>
#include <QTableWidget>
#include <KPlotObject>
#include <KPlotPoint>
#include <KPlotAxis>

#include <cmath>

using namespace std;

KRootsView::KRootsView(QWidget *parent)
			: QWidget(parent), Ui::Form(), PI(acos(-1.00000)), mInDegrees(Settings::bool_degrees()),mMaxValue(0)
{
	setupUi(this);
	settingsChanged();
	setAutoFillBackground(true);

	QList<QWidget*> l = this->findChildren<QWidget *>();
	for (int i=0;i<l.size();i++){
	    QWidget *w = l[i];
    	    if (!w->toolTip().isEmpty()) w->setToolTip(i18n(w->toolTip().toStdString().c_str()));
	    if (!w->whatsThis().isEmpty()) w->setWhatsThis(i18n(w->whatsThis().toStdString().c_str()));
	}
	
	cmdDraw->setText(i18n(cmdDraw->text().toStdString().c_str()));
	cmdClear->setText(i18n(cmdClear->text().toStdString().c_str()));

	
	connect(cmdDraw, SIGNAL(clicked()), SLOT(drawPolygon()));
	connect(cmdClear,SIGNAL(clicked()), SLOT(clear()));
	connect(complexNumber, SIGNAL(currentChanged(int)), SLOT(complexFormatChanged()));
}

KRootsView::~KRootsView()
{
}

void KRootsView::settingsChanged()
{
	graph->setBackgroundColor( Settings::col_background());
	graph->setForegroundColor( Settings::col_foreground());
	graph->setShowGrid( Settings::bool_hasgrid());
	graph->setGridColor( Settings::col_grid());
	
	if ( Settings::bool_showlabels())
	{
		graph->axis( KPlotWidget::BottomAxis )->setLabel(i18n("Real"));
		graph->axis( KPlotWidget::LeftAxis )->setLabel(i18n("Imaginary"));
	} else {
        graph->axis( KPlotWidget::BottomAxis )->setLabel(QLatin1String(""));
        graph->axis( KPlotWidget::LeftAxis )->setLabel(QLatin1String(""));
	}
	
	if (mInDegrees ^ Settings::bool_degrees())
	{
		if (mInDegrees)
			spnTheta->setValue( spnTheta->value() * PI/180);
		else 
			spnTheta->setValue( spnTheta->value() * 180/PI);
		mInDegrees = Settings::bool_degrees();
	}
}

void KRootsView::drawPolygon()
{
	if (complexNumber->currentIndex()) // == 1 is not required, GO C++!
		fillInRCISO();
	qreal radius = pow( spnRadius->value(), 1.0f/ (double)spnRoot->value());
	
	QTableWidget *data = new QTableWidget;
	data->setRowCount(spnRoot->value());
	data->setColumnCount(3);
	data->setHorizontalHeaderItem(0,new QTableWidgetItem(i18n("Theta")));
	data->setHorizontalHeaderItem(1,new QTableWidgetItem(i18n("Real")));
	data->setHorizontalHeaderItem(2,new QTableWidgetItem(i18n("Imaginary")));
	
	KPlotObject* polygon = new KPlotObject(cmdColor->color(), KPlotObject::Lines);
	
	for (int i = 0; i < spnRoot->value(); i++)
	{
		qreal theta; 
		if ( mInDegrees )
			theta = ((2 * PI * i) + (spnTheta->value() * PI/180)) / spnRoot->value();
		else
			theta = ((2 * PI * i) + spnTheta->value()) / spnRoot->value();
		
		qreal real = cos( theta ) * radius;
		qreal imaginary = sin( theta ) * radius;
		polygon->addPoint( QPointF(real,imaginary) );
		
		if ( mInDegrees )//				Perposly do not translate
            data->setItem(i,0, new QTableWidgetItem( QString(QLatin1String("%1")).arg(theta * 180/PI) ) );
		else
            data->setItem(i,0, new QTableWidgetItem( QString(QLatin1String("%1")).arg( theta ) ) );
		
        data->setItem(i,1, new QTableWidgetItem( QString(QLatin1String("%1")).arg( real ) ));
        data->setItem(i,2, new QTableWidgetItem( QString(QLatin1String("%1")).arg( imaginary ) ));
		mMaxValue = max(max(abs(real),abs(imaginary)),mMaxValue);
	}
	
	if (complexNumber->currentIndex()) //a+bi format
        tabData->addTab( data, QString(QLatin1String("(%1 + %2i)^(1/%3)"))
		.arg(spnReal->value()).arg(spnImaginary->value()).arg(spnRoot->value()));
	else
        tabData->addTab( data, QString(QLatin1String("(%1 cis %2)^(1/%3)"))
		.arg(spnRadius->value()).arg(spnTheta->value()).arg(spnRoot->value()));
	tabData->setCurrentIndex(tabData->count() - 1);
	
	polygon->addPoint(QPointF(polygon->points().at(0)->x(), polygon->points().at(0)->y()));
	graph->setLimits(-mMaxValue,mMaxValue,-mMaxValue,mMaxValue);
	graph->addPlotObject(polygon);
}

void KRootsView::clear()
{
	graph->removeAllPlotObjects();
	tabData->clear();
	graph->setLimits(-1,1,-1,1);
	mMaxValue = 0;
}

void KRootsView::fillInRCISO()
{
	spnRadius->setValue( sqrt( ( pow(spnReal->value(),2) + pow(spnImaginary->value(),2)) ));
	spnTheta->setValue( atan2( spnImaginary->value(), spnReal->value()));
}

void KRootsView::complexFormatChanged()
{
	switch (complexNumber->currentIndex())
	{
		case 0:
			fillInRCISO();
		break;
		
		case 1:
			spnReal->setValue( cos( spnTheta->value()) * spnRadius->value());
			spnImaginary->setValue( sin( spnTheta->value()) * spnRadius->value());
		break;
		
		default: break;
	}
}

#include "krootsview.moc"
