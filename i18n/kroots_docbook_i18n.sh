#!/bin/sh

# You should have poxml installed for xml2pot command

echo "Creating kroots_docbook.pot"
xml2pot ../doc/index.docbook > kroots_docbook.pot
echo "Done."

echo "Fix POT file header"
# stupid transifex
a=`cat kroots_docbook.pot|grep POT-Creation-Date|sed "s/POT-Creation-Date/PO-Revision-Date/"`
b=`echo $a|cut -d '\' --fields=1`
c=`echo $b yyy`
sed -i "s|\"PO-Revision-Date.*|$c|" kroots_docbook.pot
sed -i 's| yyy|\\n"|' kroots_docbook.pot
# update url
sed -i 's|http://bugs.kde.org|https://bitbucket.org/admsasha/kroots/issues|' kroots_docbook.pot
echo "Done."
